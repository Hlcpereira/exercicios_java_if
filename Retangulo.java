public class Retangulo {
    Ponto ponto1, ponto2, ponto3, ponto4;
    Double largura, altura;

    public Retangulo(Ponto ponto1, Ponto ponto4, Double largura, Double altura) {
        this.ponto1 = ponto1;
        this.ponto4 = ponto4;
        this.largura = largura;
        this.altura = altura;
        ponto2 = new Ponto(ponto4.getX() , 0.0);
        ponto3 = new Ponto(0.0 , ponto4.getY());
    }
}
