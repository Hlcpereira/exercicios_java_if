public class Circulo {
    Ponto ponto1, ponto2;
    Double raio1;

    public Circulo(Ponto ponto1, Ponto ponto2, Double raio2) {
        this.ponto1 = ponto1;
        this.ponto2 = ponto2;
        raio1 = raio2;
    }
}
