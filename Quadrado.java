public class Quadrado {
    Ponto ponto1, ponto2, ponto3, ponto4;
    Double lado;

    public Quadrado(Ponto ponto1, Double lado) {
        this.ponto1 = ponto1;
        this.lado = lado;
        ponto2 = new Ponto(ponto1.getX() + lado, ponto1.getY());
        ponto3 = new Ponto(ponto1.getX(), ponto1.getY() + lado);
        ponto4 = new Ponto(ponto1.getX() + lado, ponto1.getY() + lado);
    }
}
