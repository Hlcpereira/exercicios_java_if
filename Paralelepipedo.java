public class Paralelepipedo {
    Ponto3D ponto1, ponto2, ponto3, ponto4;

    public Paralelepipedo(Ponto3D ponto1, Ponto3D ponto2, Ponto3D ponto3, Ponto3D ponto4) {
        super();
        this.ponto1 = ponto1;
        this.ponto2 = ponto2;
        this.ponto3 = ponto3;
        this.ponto4 = ponto4;
    }
}
